﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIsometric : MonoBehaviour
{
    // Start is called before the first frame update
    Rigidbody2D rigidbody;
    [SerializeField] float speed;
    Animator animator;


    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
    }

    private void Move()
    {
        



        float inputX = Input.GetAxis("Horizontal");
        float inputY = Input.GetAxis("Vertical");
        rigidbody.velocity = new Vector2(inputX, inputY) * speed;

        if (Mathf.Abs(inputX) > 0.1f  || Mathf.Abs(inputY) > 0.1f)
        {
            animator.SetFloat("SpeedX", inputX);
        }
        if (Mathf.Abs(inputY) > 0.1f || Mathf.Abs(inputX) > 0.1f)
        {
            animator.SetFloat("SpeedY", inputY);
        }

    }
}
