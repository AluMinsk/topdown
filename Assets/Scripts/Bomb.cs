﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomb : MonoBehaviour
{
    [SerializeField] float explodeDelay = 2f;
    [Range(0.4f, 10)][SerializeField] float explodeRadius = 2f;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Explode());
    }

    IEnumerator Explode()
    {
        yield return new WaitForSeconds(explodeDelay);

        int layerMask = LayerMask.GetMask("Enemy", "EnemyBullet");
        Collider2D[] objectsToDestroy = Physics2D.OverlapCircleAll(transform.position, explodeRadius, layerMask);
        foreach (Collider2D col in objectsToDestroy)
        {
            Destroy(col.gameObject);
        }
        Destroy(gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, explodeRadius);
    }
}
