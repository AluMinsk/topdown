﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pathfinding;

public class EnemyController : MonoBehaviour
{
    public event Action<int, int> OnHealthChanged = delegate {};


    [SerializeField] int maxhealth = 100;
    [SerializeField] float fireRate = 1f;
    [SerializeField] Transform shootPosition;
    [SerializeField] GameObject bullet;
    [SerializeField] [Tooltip("Distance to start following")]float moveDistance;
    [SerializeField] float shootDistance;
    [SerializeField] float hearDistance;
    [SerializeField] float speed;
    [SerializeField] float viewAngle = 60f;

    int health;

    PlayerController player;
    EnemyState currentState;

    float distance;

    Coroutine enemyShootingCoroutine;
    Animator animator;
    AIPath aiPath;


    enum EnemyState
    {
        STAND,
        MOVE,
        SHOOT
    }

    //int STATE_STAND = 1;
    //int STATE_MOVE = 2;
    //int STATE_SHOOT = 3;


    // Start is called before the first frame update
    void Start()
    {
        health = maxhealth;
        player = FindObjectOfType<PlayerController>();
        currentState = EnemyState.STAND;
        animator = GetComponentInChildren<Animator>();
        aiPath = GetComponent<AIPath>();
        aiPath.enabled = false;
        aiPath.maxSpeed = speed;

    }

    // Update is called once per frame
    void Update()
    {
        switch(currentState)
        {
            case EnemyState.STAND:
                //do STAND
                DoStand();
                break;
            case EnemyState.MOVE:
                //do MOVE
                DoMove();
                break;
            case EnemyState.SHOOT:
                DoShoot();
                //do SHOOT
                break;
            default:
                //do default action
                break;


        }

        //Rotate();
    }

    private void DoShoot()
    {
        
        Rotate();
        Shoot();
        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (distance > shootDistance)
        {
            currentState = EnemyState.MOVE;
            StopShooting();
            animator.SetTrigger("Move");
            aiPath.enabled = true;


        }
      

    }

    private void DoMove()
    {
        Rotate();
        EnemyMove();

        float distance = Vector2.Distance(player.transform.position, transform.position);
        if (distance < shootDistance)
        {
            EnemyStop();
            currentState = EnemyState.SHOOT;
            animator.SetTrigger("Shoot");
            aiPath.enabled = false;

        }
      

    }

    private void DoStand()
    {
        float distance = Vector2.Distance(player.transform.position, transform.position);
        Vector2 direction = player.transform.position - transform.position;
        int layerMask = LayerMask.GetMask("Obstacles");
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, distance, layerMask);

        if (distance < hearDistance && hit.collider == null)
        {

            currentState = EnemyState.MOVE;
            aiPath.enabled = true;
            animator.SetTrigger("Move");
        }

        else if (distance < moveDistance)
        {
            
            
            
            float angle = Vector2.Angle(transform.up, direction);

            
            if (angle < viewAngle/2)
            {
               
                
                Debug.DrawRay(transform.position, direction);
               

                if (hit.collider == null)
                {
                    currentState = EnemyState.MOVE;
                    aiPath.enabled = true;
                    animator.SetTrigger("Move");
                }
            }
            //currentState = EnemyState.MOVE;
            //animator.SetTrigger("Move");
        }
        

    }

    private void Rotate()
    {
        //Vector substraction
        Vector2 direction = player.transform.position - transform.position;
        transform.up = direction;

        //GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;
    }


    private void EnemyMove()
    {
        Vector2 direction = player.transform.position - transform.position;
       // transform.up = direction;
        GetComponent<Rigidbody2D>().velocity = direction.normalized * speed;

    }

    private void EnemyStop()
    {
        Vector2 direction = player.transform.position - transform.position;
        // transform.up = direction;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            //direction.normalized * 0;
    }
    private void Shoot()
    {
        if (enemyShootingCoroutine == null)
        {
            enemyShootingCoroutine = StartCoroutine(Shooting());
        }
    }

    private void StopShooting()
    {
        StopCoroutine(enemyShootingCoroutine);
        enemyShootingCoroutine = null;
    }

    IEnumerator Shooting()
    {
       
        while (true)
        {
            Instantiate(bullet, shootPosition.position, transform.rotation);
            yield return new WaitForSeconds(fireRate);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageDealer damageDealer = collision.GetComponent<DamageDealer>();
        if (damageDealer != null)
        {
            health -= damageDealer.GetDamage();
            OnHealthChanged(health, maxhealth);
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, shootDistance);

        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, moveDistance);

        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(transform.position, hearDistance);

        Gizmos.color = Color.green;
        //Quaternion rotation = Quaternion.AngleAxis(viewAngle/2, transform.forward);
        //Gizmos.DrawRay(transform.position, transform.up * rotation);

        Vector3 rotatedVector = Quaternion.AngleAxis(viewAngle / 2, transform.forward) * transform.up * moveDistance;
        Vector3 rotatedVector2 = Quaternion.AngleAxis(-viewAngle / 2, transform.forward) * transform.up * moveDistance;

        Gizmos.DrawRay(transform.position, rotatedVector);
        Gizmos.DrawRay(transform.position, rotatedVector2);







    }
}
