﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyCanvas : MonoBehaviour

{
    [SerializeField] float healthUpdateTime = 0.5f;
    [SerializeField] Image healthBar;
    [SerializeField] EnemyController controller;

    // Start is called before the first frame update
    void Start()
    {
        controller.OnHealthChanged += HandleHealthChange;
    }

    void HandleHealthChange(int health, int maxhealth)

    {

        float nowHealth = (float)health / (float)maxhealth;
        // print("health changed event!" + nowHealth);
        //healthBar.fillAmount = nowHealth;
        StartCoroutine(UpdateHealthValue(nowHealth));
    }

    IEnumerator UpdateHealthValue(float nowHealth)
    {
        float previousPercent = healthBar.fillAmount;
        float elapsedTime = 0;

        while (elapsedTime < healthUpdateTime)
        {

            elapsedTime += Time.deltaTime;

            healthBar.fillAmount = Mathf.Lerp(previousPercent, nowHealth, elapsedTime/healthUpdateTime);
            //healthBar.fillAmount -= 0.01f;
            yield return null;

                 
        }
        //healthBar.fillAmount = nowHealth;
    }


    // Update is called once per frame
    void Update()
    {
        transform.localRotation = Quaternion.Euler(0, 0, -transform.parent.eulerAngles.z);
    }
}
